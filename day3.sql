CREATE TABLE student ( id VARCHAR(20) PRIMARY KEY, name VARCHAR(50) NOT NULL, faculty VARCHAR(20), section char, result BOOLEAN);

ALTER TABLE result DROP COLUMN section;
ALTER TABLE result CHANGE student_rollno student_id VARCHAR(20);
ALTER TABLE result ADD COLUMN full_marks int DEFAULT(100) AFTER examdate;
ALTER TABLE result DROP COLUMN percentage, DROP COLUMN passed;

DELETE FROM result WHERE marks_obtained = 0;

INSERT INTO student(id, name, faculty, section) VALUES
                    ('076BIM443', 'student one', 'BIM', 'A'),
                    ('076BIM444', 'student two', 'BIM', 'A');
INSERT INTO result VALUES (NULL, 'Database', '076BIM443', NULL, 100, 78),
                        (NULL, 'Mathematics', '076BIM443', NULL, 100, 85);

SELECT * FROM student, result WHERE student.id = result.student_id;
